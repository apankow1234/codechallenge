var gl;
var globalTimer;
var globalCamera          =  null;
var globalCameraCtrl      =  null;
var globalModel           =  null;
var globalModel2          =  null;
var globalShader          =  null;
var globalGridShader      =  null;
var globalGridModel       =  null;
var globalParticleShader  =  null;
var globalParticles       =  [];

var globalParticleEmitter =  null;
var globalParticleEmitter2 =  null;

function onRender( deltaTime ) {
	globalCamera.updateViewMatrix();
	gl.refresh();

	globalGridShader.activate()
		.setCamera(   globalCamera.viewMatrix     )
		.renderModel( globalGridModel.prerender() );
	
	if( globalParticleEmitter != null ) {
		globalParticleShader.activate()
		.setCamera(   globalCamera.viewMatrix )
		.renderModel( globalParticleEmitter.prerender( deltaTime ).particles );
	}
	if( globalParticleEmitter2 != null ) {
		globalParticleShader.activate()
		.setCamera(   globalCamera.viewMatrix )
		.renderModel( globalParticleEmitter2.prerender( deltaTime ).particles );
	}
}

function main()
{
	/* GL Init */
	let viewportID = 'viewport';
	gl = GLInstance( viewportID ).resize( 720, 480 ).refresh();

	/* Construct Scene */
	globalCamera = new Camera( gl );
	globalCamera.xform
		.setRotation( 0, 45, 0 )
		.setPosition( 0, 5,  10 );
	globalCameraCtrl = new CameraController( gl, globalCamera );
	let pMat4        = globalCamera.projectionMatrix;

	/* Load Models */
	globalShader         = new ParticleShader( gl, pMat4 );
	globalParticleShader = new ParticleShader( gl, pMat4 );
	globalGridShader     = new GridAxisShader( gl, pMat4 );
	globalGridModel      = Primitive.GridAxis.buildMesh( gl );
	let smokeShape       = Primitive.GridAxis.buildMesh( gl );
	
	Primitive.Quad.buildMesh( gl, 'quad', .1 );
	for( let i=0; i < 100; i++ ) {
		let newParticle  = new Particle( i, 'particle', gl.meshCache["quad"] );
		newParticle.acceleration = new Vec3( 
			( Math.random() - 0.5 ) * .5, 
			( Math.random() * 5 ), 
			( Math.random() - 0.5 ) * .5 
		);
		globalParticles.push( newParticle );
	}

	globalParticleEmitter = new Emitter( gl, 'test', smokeShape );
	globalParticleEmitter.xform.setPosition( 0, 0.1, 0 );
	globalParticleEmitter.life          = 1.0;
	globalParticleEmitter.lifeRange     = 0.15;

	globalParticleEmitter2 = new Emitter( gl )
	globalParticleEmitter2.xform.setPosition( 0, 0.1, 0 );
	globalParticleEmitter2.gravity      = 5.0;
	globalParticleEmitter2.life         = 1.0;
	globalParticleEmitter2.lifeRange    = 0.15;
	globalParticleEmitter2.size         = 0.05;
	globalParticleEmitter2.sizeRange    = 0.01;
	globalParticleEmitter2.maxParticles = 150;

	/* Render */
	globalTimer = new TimerLoop( 1, onRender ).start(); /* callback function receives deltaTime param */
	return 0;
}