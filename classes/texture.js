var allTextures = [];

class Texture {
	constructor( gl, filename, name = "" ) {
		this.name             = name;
		this.tex              = gl.createTexture;
		this.tex.image        = new Image();
		this.tex.image.onload = () => {
			gl.bindTexture(   gl.TEXTURE_2D, this.tex );
			gl.pixelStorei(   gl.UNPACK_FLIP_Y_WEBGL, true );
			gl.texImage2D(    gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.tex.image );
			gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST ); /* not `max`... it is `mag` */
			gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST );
			gl.bindTexture(   gl.TEXTURE_2D, null );
		}
		this.tex.image.src    = filename;
		allTextures.push(this);
	}

	get img() {
		return this.tex.image;
	}
}