class ShaderUtil {
	
	static getShaderByDomId( ID ) {
		var scriptContainer = document.getElementById( ID );
		if( !scriptContainer || scriptContainer.text == "" ) {
			console.log( "Element with ID=" + ID + ": Did not contain a usable shader.");
			return;
		}

		return scriptContainer.text;
	}


	static compileShader( gl, source, type ) {
		// console.log( type );
		var shader = gl.createShader( type );
		gl.shaderSource(  shader, source );
		gl.compileShader( shader );


		/**
		* getShaderParameter( whichShaderToCheck, whichStatusToCheck )
		*/
		if( !gl.getShaderParameter( shader, gl.COMPILE_STATUS ) ) {
			console.error( "Shader Compile Fail: " + gl.getShaderInfoLog( shader ) );
			gl.deleteShader( shader ); /* Toss the fail into the bin */
			return null;
		}

		return shader;
	}


	static compileProgram( gl, vShader, fShader, validate = false ) {
		var program = gl.createProgram();
		gl.attachShader( program, vShader );
		gl.attachShader( program, fShader );

		gl.bindAttribLocation( program, ATTR_POSITION_ADDR, ATTR_POSITION_NAME );
		gl.bindAttribLocation( program, ATTR_NORMAL_ADDR,   ATTR_NORMAL_NAME   );
		gl.bindAttribLocation( program, ATTR_UV_ADDR,       ATTR_UV_NAME       );

		gl.linkProgram( program );


		/**
		* getProgramParameter( whichProgramToCheck, whichStatusToCheck )
		*/
		if( !gl.getProgramParameter( program, gl.LINK_STATUS ) ) {
			console.error( "Program Linking Fail: " + gl.getProgramInfoLog( program ) );
			gl.deleteProgram( program ); /* Toss the fail into the bin */
			return null;
		}

		if( validate ) {
			gl.validateProgram( program );
			/**
			* getProgramParameter( whichProgramToCheck, whichStatusToCheck )
			*/
			if( !gl.getProgramParameter( program, gl.VALIDATE_STATUS ) ) {
				console.error( "Program Validation Fail: " + gl.getProgramInfoLog( program ) );
				gl.deleteProgram( program ); /* Toss the fail into the bin */
				return null;
			}
		}

		/**
		* Program has been compiled, raw resources can be disposed of.
		* Could potentially need to detach before allowed to delete gl.detachShader(shader);
		*/
		gl.detachShader( program, fShader );
		gl.detachShader( program, vShader );
		gl.deleteShader( fShader );
		gl.deleteShader( vShader );

		return program;
	}

	static getShadersFromDOM( gl, vShaderTxtID, fShaderTxtID, validate = false ) {
		/* Get shader source text */
		/* Go around the house/ apartment and see if everyone is in the mood for pizza */
		var vShaderTxt = ShaderUtil.getShaderByDomId( vShaderTxtID ); 
		if(!vShaderTxt) return; /* Not worth it if nobody is home */
		var fShaderTxt = ShaderUtil.getShaderByDomId( fShaderTxtID ); 
		if(!fShaderTxt) return; /* Not worth it if nobody is home */

		return ShaderUtil.compileProgramFromText( gl, vShaderTxt, fShaderTxt, validate );
	}

	static compileProgramFromText( gl, vShaderTxt, fShaderTxt, validate = false ) {
		/* Compile shaders and validate */
		/* Confirm that pizza is what will be ordered and not steaks and potatos */
		var vShader = ShaderUtil.compileShader( gl, vShaderTxt, gl.VERTEX_SHADER   ); 
		if(!vShader) return; /* Not worth it if someone is going to throw a tantrum */
		var fShader = ShaderUtil.compileShader( gl, fShaderTxt, gl.FRAGMENT_SHADER ); 
		if(!fShader) {
			gl.deleteShader( vShader ); /* Will have been created but won't be used... goodbye! */
			return;
		}; /* Not worth it if someone is going to throw a tantrum */

		/* Make GPU runnable program - and validate */
		/* As the one who is paying, you're making the decision for pizza */
		return ShaderUtil.compileProgram( gl, vShader, fShader, true );
	}

	static getStdAttribAddrs( gl, program ) {
		return {
			'pos'  : gl.getAttribLocation( program, ATTR_POSITION_NAME ),
			'norm' : gl.getAttribLocation( program, ATTR_NORMAL_NAME   ),
			'uv'   : gl.getAttribLocation( program, ATTR_UV_NAME       )
		};
	}

	static getStdUniformAddrs( gl, program ) {
		return {
			'persp'   : gl.getUniformLocation( program, "uPerspMatrix"     ),
			'model'   : gl.getUniformLocation( program, "uModelViewMatrix" ),
			'camera'  : gl.getUniformLocation( program, "uCameraMatrix"    ),
			'mainTex' : gl.getUniformLocation( program, "uMainTexture"     )
		};
	}
}