/* Based on `Fun with WebGL 2.0` but with obvious liberties */
class TimerLoop {
	constructor( miliseconds, callback, verbose=false ) {
		var my         = this;
		this.lastTime  = 0.0;
		this.callback  = callback;
		this.isRunning = false;
		this.every     = miliseconds;
		this.fps       = 0;
		this.run = () => {
			let rn     = performance.now();
			let delta  = ( rn - my.lastTime );
			let dt     = delta / 1000.0;

			if(delta >= my.every) {
				my.fps      = Math.floor( 1.0 / dt );
				if(verbose)
					console.log( "fps: ", my.fps );
				my.lastTime = rn;
				my.callback( dt );
			}

			if( my.isRunning ) {
				window.requestAnimationFrame( my.run );
			}
		}
	}

	start() {
		this.isRunning = true;
		this.lastTime  = performance.now();
		window.requestAnimationFrame( this.run );
		return this;
	}

	stop() {
		this.isRunning = false;
	}
}