Primitive.GridAxis = class {

	static buildMesh( gl ) {
		let name = XformNode.validateName( "sceneGrid" );
		return new ShapeNode( name, Primitive.GridAxis.createMesh( gl, name ) );
	}

	static getGridVerts( size = 1.8, numDivisions = 10.0 ) {

		var verts          = [];
		var step           = size / numDivisions;
		var half           = size / 2;
		let p;
		for( let i=0; i <= numDivisions; i++ ) {
			p = -half + ( i * step );
			verts.push(   p    ); //x1
			verts.push(   0    ); //y1
			verts.push(  half  ); //z1
			verts.push(   0    ); //c0 - white

			verts.push(   p    ); //x2
			verts.push(   0    ); //y2
			verts.push( -half  ); //z2
			verts.push(   0    ); //c0 - white

			p = half - ( i * step );
			verts.push( -half  ); //x1
			verts.push(   0    ); //y1
			verts.push(   p    ); //z1
			verts.push(   0    ); //c0 - white

			verts.push(  half  ); //x2
			verts.push(   0    ); //y2
			verts.push(   p    ); //z2
			verts.push(   0    ); //c0 - white

		}
		return verts;
	}

	static getAxisVerts( size = 1.8 ) {
		let len   = size * 0.625; 
		var verts = [];
		//x axis
		verts.push(   0    );     //x1
		verts.push(   0    );     //y1
		verts.push(   0    );     //z1
		verts.push(   1    );     //c2 - red

		verts.push(   len  );     //x2
		verts.push(   0    );     //y2
		verts.push(   0    );     //z2
		verts.push(   1    );     //c2 - red

		//y axis
		verts.push(   0    );     //x1
		verts.push(   0    );     //y1
		verts.push(   0    );     //z1
		verts.push(   2    );     //c2 - green

		verts.push(   0    );     //x2
		verts.push(   len  );     //y2
		verts.push(   0    );     //z2
		verts.push(   2    );     //c2 - green

		//z axis
		verts.push(   0    );     //x1
		verts.push(   0    );     //y1
		verts.push(   0    );     //z1
		verts.push(   3    );     //c2 - blue

		verts.push(   0    );     //x2
		verts.push(   0    );     //y2
		verts.push(   len  );     //z2
		verts.push(   3    );     //c2 - blue
		return verts;
	}

	static createMesh( gl, name ) {

		let gridSize = 1.8;
		let verts = [];
		verts = verts.concat( Primitive.GridAxis.getGridVerts( gridSize, 10.0 ) );
		verts = verts.concat( Primitive.GridAxis.getAxisVerts( gridSize ) );
		// console.log( verts)

		var attrColorAddr  = 4;
		var mesh           = {
			  drawMode     : gl.LINES
			, vao          : gl.createVertexArray()
			, vertexBuffer : gl.createBuffer()
		};
		var stride;

		mesh.floatsPerVert = 4;
		mesh.vertexCount   = verts.length / mesh.floatsPerVert;
		stride             = Float32Array.BYTES_PER_ELEMENT * mesh.floatsPerVert;

		// mesh.vertexBuffer = gl.createBuffer();
		gl.bindVertexArray( mesh.vao );
		gl.bindBuffer( gl.ARRAY_BUFFER, mesh.vertexBuffer );
		gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( verts ), gl.STATIC_DRAW );
		gl.enableVertexAttribArray( ATTR_POSITION_ADDR );
		gl.enableVertexAttribArray( attrColorAddr      );

		gl.vertexAttribPointer(
			ATTR_POSITION_ADDR
			, 3
			, gl.FLOAT
			, false
			, stride
			, 0
		);

		gl.vertexAttribPointer(
			attrColorAddr
			, 1
			, gl.FLOAT
			, false
			, stride
			, Float32Array.BYTES_PER_ELEMENT * 3
		);

		/* Lock up when you're done so nothing else can be slipped in later */
		gl.bindVertexArray( null );
		gl.bindBuffer( gl.ARRAY_BUFFER, null );

		gl.meshCache[ name ] = mesh;
		// console.log( gl.meshCache );

		return mesh
	}

}