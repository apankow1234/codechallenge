class Camera extends ShapeNode {
	
	constructor( gl = null, fieldOfView = 45, clipNear = 0.1, clipFar = 100.0 ) {
		super( "persp" );

		let ratio = gl.canvas.width / gl.canvas.height;
		// console.log( fieldOfView, ratio, clipNear, clipFar );
		this.projectionMatrix = Mat4.persp( fieldOfView, ratio, clipNear, clipFar );
		this.viewMatrix = new Float32Array(16);
		// console.log( this.projectionMatrix, this.viewMatrix );

		this.mode = Camera.MODE_ORBIT;
	}



	panX( amount ) {
		if( this.mode == Camera.MODE_ORBIT )
			return;
		this.updateViewMatrix();
		this.xform.addVPosition( this.xform.right.mult( amount ) );
	}

	panY( amount ) {
		this.updateViewMatrix();
		let amt = new Vec3( 0.0, (amount * this.xform.up.y), 0.0 );
		if( this.mode != Camera.MODE_ORBIT ) {
			amt.x = amount * this.xform.up.x;
			amt.z = amount * this.xform.up.z;
		}
		this.xform.addVPosition( amt );
	}

	panZ( amount ) {
		this.updateViewMatrix();
		if( this.mode == Camera.MODE_ORBIT ) {
			this.xform.addPosition( 0, 0, amount );
		} else {
			this.xform.addVPosition( this.xform.forward.mult( amount ) );
		}
	}


	updateViewMatrix() {
		if( this.mode == Camera.MODE_FREE ) {
			this.xform.matrix.reset()
				.vmove(   this.xform.position )
				.rotateX( this.xform.rotation.x * XformNode.deg2Rad )
				.rotateY( this.xform.rotation.y * XformNode.deg2Rad );
		} else {
			this.xform.matrix.reset()
				.rotateX( this.xform.rotation.x * XformNode.deg2Rad )
				.rotateY( this.xform.rotation.y * XformNode.deg2Rad )
				.vmove(   this.xform.position );
		}

		this.xform.updateDirection();

		Mat4.invert( this.viewMatrix, this.xform.matrix.c );
		return this.viewMatrix;
	}
}

Camera.MODE_FREE  = 0;
Camera.MODE_ORBIT = 1;