Primitive.Quad = class {

    static buildMesh( gl, name = "quad", size = 1.0 ) {
		name = XformNode.validateName( name );
		return new ShapeNode( name, Primitive.Quad.createMesh( gl, name, size ) );
    }

    static getVerts( size = 1.0 ) {
        let pos = 0.5 * size;
        let neg = -pos;
        let verts = [
            neg, pos, 0,
            neg, neg, 0,
            pos, neg, 0,
            pos, pos, 0
        ];
        // console.log( "verts", verts );
        return new Float32Array(verts);
    }

    static createMesh( gl, name = "quad", size = 1.0 ) {
        let indices = [
            0, 1, 2,
            2, 3, 0
        ];
        let verts = Primitive.Quad.getVerts( size );
        let UVs = [
            0, 0, 
            0, 1, 
            1, 1, 
            1, 0 
        ];

        let mesh        = gl.createMeshVAO( name, indices, verts, null, UVs );
        mesh.noCulling  = true;
        mesh.doBlending = true;
        return mesh
    }
};