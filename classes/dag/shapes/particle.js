class Particle extends ShapeNode {
	
	constructor( ID, name = "particle", meshData = null ) {
		/* Particle Items */
		let newName = 'particle' + ID.toString();
		// console.log( newName );
		super( newName, meshData );
		this.id            = ID;
		// console.log( this.id, this.name );
		this.active        = true;
		this.lastTime      = -1;

		/* Particle Settings */
		this.totalLife     = Math.random() *  2.0;
		this.bounciness    = 0.9;
		this.velocity      = new Vec3( 0, 1, 0 );
		this.acceleration  = new Vec3();

		/* Particle Render Settings */
		this.color         = new Vec3();
		this.rotation      = new Vec3();
		this.alpha         = 1.0;
		this.size          = 1.0;
	}

	prerender( time ) {
		if(this.active == false)
			return this;

		if( this.lastTime == -1 )
			this.lastTime = time;

		let d               = ( time - this.lastTime );
		this.totalLife     -= d;
		this.xform.rotation.x += d;
		this.xform.rotation.y += d;
		this.xform.rotation.z += d;
		// if( this.id == 0 ) console.log( "life ", this.totalLife )

		if( this.totalLife <= 0.0 ) {
			this.active = false;
			// this.xform.removeNode( true );
		}

		this.velocity.x       += this.acceleration.x * d;
		this.velocity.y       += this.acceleration.y * d;
		this.velocity.z       += this.acceleration.z * d;
		this.xform.position.x += this.velocity.x     * d;
		this.xform.position.y += this.velocity.y     * d;
		this.xform.position.z += this.velocity.z     * d;
		this.xform.scale.x    += d;
		this.xform.scale.y    += d;
		this.xform.scale.z    += d;

		if( this.xform.position.y < 0.0 ) {
			this.velocity.y       *= -this.bounciness;
			this.xform.position.y  = 0.0;
		}

		// const fadeTime = 0.2;
		// if( this.totalLife - this.life < fadeTime ) {
		// 	// glColor4f(this.color.x, this.color.y, this.color.z, (this.totalLife - this.life) / fadeTime * this.alpha);  // need to port OpenGL1 for webGL
		// } else if( this.life < 1.0 ) {
		// 	// glColor4f(this.color.x, this.color.y, this.color.z, this.life * this.alpha);                                // need to port OpenGL1 for webGL
		// } else {
		// 	// glColor4f(this.color.x, this.color.y, this.color.z, this.alpha);                                            // need to port OpenGL1 for webGL
		// }



		// // this.lastTime = time;
		// this.xform.position.x += this.acceleration.x * Math.random();
		// this.xform.position.y += this.acceleration.y * Math.random();
		// this.xform.position.z += this.acceleration.z * Math.random();

		// this.xform.matrix.resetRotation();
		this.xform.updateMatrix();
		return this;
	}
	

}