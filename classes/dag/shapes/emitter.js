class Emitter extends ShapeNode  {
	
	constructor( gl, name = "particleEmitter", shapeNode = null ) {
		/* Engine Items */
		super( name );
		this.lastTime      = -1; // init value
		this.particleShape = shapeNode || Primitive.Quad.buildMesh( gl, 'spriteMesh', 1.0 );
		this.maxParticles  = 500; 

		/* Engine Settings */
		this.emissionRate   = 30.0;
		this.emissionRadius = 0.0;
		this.spread         = 5.0;

		/* Hardcoded Forces */
		this.gravity        = 0.50;
		this.wind           = new Vec3( 0, 0.5, 0 );

		/* Particle Birth Settings */
		this.particles      = [];
		this.totalParticles = 0;
		this.life           = 2.0;
		this.lifeRange      = 0.5;
		this.size           = 0.15;
		this.sizeRange      = 0.05;
		this.Texture        = null /*new Texture()*/;
		this.saturation     = 1.0;
		this.alpha          = 0.5;
	}

	static frand( start = 0, end = 1 ) {
		const RAND_MAX = 1;
		let num = Math.random() / RAND_MAX;
		return ( start + ( end - start ) * num );
	}

	applyForces( particleShapeNode ) {
		// this.wind = new Vec3( this.wind * Math.random(), this.wind * Math.random(), this.wind * Math.random() );
		particleShapeNode.acceleration.x += this.wind.x;
		particleShapeNode.acceleration.y += this.wind.y - this.gravity;
		particleShapeNode.acceleration.z += this.wind.z;
		if( particleShapeNode.xform.position.y < -1.0 )
			particleShapeNode.active = false;
	}

	prerender( time ) {
		// if(this.Texture == null)
		// 	return;
		if( this.lastTime == -1 )
			this.lastTime = time; // initialization

		/* time and lastTime are milliseconds... cast up to seconds and then multiply by emission rate */
		let numEmission = parseInt( ( time - this.lastTime ) * 100.0 * this.emissionRate );
		// if( this.lastTime != time ) console.log( "delta",( time - this.lastTime ) * 100.0, 'numEmission', numEmission )

		// for( let i=0; i < numEmission; i++ ) {
		if( this.particles.length < this.maxParticles ) {
			this.addParticle();
		}
		// }

		// if( numEmission > 0 )
		// 	this.lastTime = time;

		// glDisable(GL_LIGHTING);                               // need to port OpenGL1 for webGL
		// gl.disable( gl.LIGTHING );
		// glDisable(GL_DEPTH_TEST);                             // need to port OpenGL1 for webGL
		// gl.disable( gl.DEPTH_TEST );
 
		// this.xform.updateMatrix();
		// glEnable(GL_TEXTURE_2D);                              // need to port OpenGL1 for webGL
		// glBindTexture(GL_TEXTURE_2D, this.Texture['texID']);  // need to port OpenGL1 for webGL

		this.particles = this.particles.filter( el => el.active );
		// console.log( this.particles );
		for( let it=0; it < this.particles.length; it++ ) {
			let p =  this.particles[ it ];
			this.applyForces( p );
			// p.alpha =  this.alpha;
			// p.rotation =  this.rotation;

			p.prerender( time );
		}

		// this.xform.updateMatrix();

		return this;
	}

	// setTexture( texture ) {
	// 	return;
	// }

	addParticle( name = "particle" ) {
		// this.totalParticles += 1;
		let pos = this.xform.position;
		let p = new Particle( this.totalParticles++, name, this.particleShape.mesh );
		p.xform.setPosition( 
			pos.x * Emitter.frand( -this.spread, this.spread ), 
			pos.y, 
			pos.z * Emitter.frand( -this.spread, this.spread )
		);
		// console.log( p.name );
		p.totalLife = Emitter.frand( this.life - this.lifeRange, this.life + this.lifeRange );

		p.velocity.x = Emitter.frand( -this.spread, this.spread );
		p.velocity.y = Emitter.frand( 10, 20 );
		p.velocity.z = Emitter.frand( -this.spread, this.spread );

		p.acceleration.y   = -this.gravity;
		p.size = Emitter.frand(  this.size - this.sizeRange, this.size + this.sizeRange );
		p.xform.position.x = Emitter.frand( -this.emissionRadius, this.emissionRadius );
		p.xform.position.z = Emitter.frand( -this.emissionRadius, this.emissionRadius );
		p.xform.setScale( p.size, p.size, p.size );
		// let inverseSaturation = 1.0 - this.saturation;
		// let r   = Emitter.frand() * this.saturation + inverseSaturation;
		// let g   = Emitter.frand() * this.saturation + inverseSaturation;
		// let b   = Emitter.frand() * this.saturation + inverseSaturation;
		// p.color = new Vec3( r, g, b );

		this.particles.push( p );
	}
}