class XformNode extends Node {
	
	constructor( name ) {
		super( name );
		this.name      = XformNode.validateName( name ); // must do before push unless always numbered is desired
		XformNode.AllNodes.push( this );
		XformNode.Roots.push( this );

		this.position  = new Vec3( 0, 0, 0 );
		this.scale     = new Vec3( 1, 1, 1 );
		this.rotation  = new Vec3( 0, 0, 0 );
		this.matrix    = new Mat4();
		this.matNormal = new Float32Array( 9 );

		this.forward   = new Vec4();
		this.up        = new Vec4();
		this.right     = new Vec4();

		this.updateMatrix();

		this.parent;
		this.children  = [];

		this.shape;
	}

	setShape( shapeNode ) { this.shape = shapeNode;    return this; }
	setPosition( x, y, z )   { this.position.set( x, y, z ); return this; }
	setScale(    x, y, z )   { this.scale.set(    x, y, z ); return this; }
	setRotation( x, y, z )   { this.rotation.set( x, y, z ); return this; }

	addPosition( x, y, z )   { this.position.x += x; this.position.y += y; this.position.z += z; return this; }
	addScale(    x, y, z )   { this.scale.x    += x; this.scale.y    += y; this.scale.z    += z; return this; }
	addRotation( x, y, z )   { this.rotation.x += x; this.rotation.y += y; this.rotation.z += z; return this; }
	addVPosition( vec3 )     { return this.addPosition( vec3.x, vec3.y, vec3.z ); }
	addVScale(    vec3 )     { return this.addScale(    vec3.x, vec3.y, vec3.z ); }
	addVRotation( vec3 )     { return this.addRotation( vec3.x, vec3.y, vec3.z ); }

	updateMatrix() {
			this.matrix.reset()
				.vmove(   this.position )
				.rotateX( this.rotation.x * XformNode.deg2Rad )
				.rotateZ( this.rotation.z * XformNode.deg2Rad )
				.rotateY( this.rotation.y * XformNode.deg2Rad )
				.vscale(  this.scale );

		Mat4.makeNormalMat3( this.matNormal, this.matrix.c );

		this.updateDirection();
		// console.log( this.rotation.z * XformNode.deg2Rad );

		return this.matrix.c;
	}

	updateDirection() {
		Mat4.xformVec4ByMat4( this.forward.c, [ 0, 0, 1, 0 ], this.matrix.c );
		Mat4.xformVec4ByMat4( this.up.c,      [ 0, 1, 0, 0 ], this.matrix.c );
		Mat4.xformVec4ByMat4( this.right.c,   [ 1, 0, 0, 0 ], this.matrix.c );
		return this;
	}

	reset() {
		this.position.set( 0, 0, 0 );
		this.scale.set(    1, 1, 1 );
		this.rotation.set( 0, 0, 0 );
		return this;
	}

	setParent( parent ) {
		if( this.parent == null ) {
			/* Remove from roots cause has parent */
			let idx = XformNode.Roots.indexOf( this );
			if(idx >= 0)
				XformNode.Roots.splice( idx, 1);
		} else {
			let idx = this.parent.children.indexOf( this );
			if(idx >= 0)
				this.parent.children.splice( idx, 1);
		}
		this.parent = parent;
		if( parent == null )
			XformNode.Roots.push( this );
		else
			parent.children.push( this );
		return this;
	}

	findNode( name ) {
		if( this.name == name ) {
			return this;
		} else if( this.shape && this.shape.name == name ) {
			return this.shape;
		} else {
			for( let i=0; i < this.children.length; i++ ) {
				return this.children[ i ].findNode( name );
			}
			return null;
		}
	}

	showTree( depth = 0 ) {
		let padding = "";
		padding = padding.padStart( depth * 2, '-' );
		let shapeName = "";
		if( this.shape )
			shapeName = "  [ " + this.shape.name + " ]";
		console.log( padding + this.name + shapeName );
		for( let c=0; c < this.children.length; c++ ) {
			this.children[c].showTree( depth + 1 );
		}
	}

	showAll() {
		this.showTree( 0 );
	}

	removeNode() {
		if( this.parent != null ) {
			let idx = this.parent.children.indexOf( this );
			if(idx >= 0)
				this.parent.children.splice( idx, 1 );
		} else {
			let idx = XformNode.Roots.indexOf( this );
			if(idx >= 0)
				XformNode.Roots.splice( idx, 1);
		}
	}

	get viewMatrix()   {  return this.matrix.c;   }
	get normalMatrix() {  return this.matNormal;  }

	static parent() {
		/* First argument will be parent, all others will be children */
		if( arguments.length < 2 )
			return null;

		let parentNode;
		if( arguments[0] instanceof XformNode )
			parentNode = arguments[ 0 ];
		else if( arguments[0] instanceof ShapeNode )
			parentNode = arguments[ 0 ].xform;
		else
			return null;

		for( let i=1; i < arguments.length; i++ ) {
			let childNode;
			if( arguments[ i ] instanceof XformNode )
				childNode = arguments[ i ];
			else if( arguments[ i ] instanceof ShapeNode )
				childNode = arguments[ i ].xform;
			else
				childNode = new XformNode( arguments[ i ] );
			childNode.setParent( parentNode );
		}
		return parentNode;
	}

	static group() {
		/* All arguments will be grouped under a newly created parent XFormNode */
		let newName = "";
		let objects = [...arguments];

		if( objects[ 0 ] instanceof XformNode ) {
			newName   = "group";
		} else {
			newName   = objects.shift();
		}
		let groupNode = new XformNode( newName );

		for( let i=0; i < objects.length; i++ ) {
			if( objects[ i ] instanceof XformNode ) {
				objects[ i ].setParent( groupNode );
			} else if( objects[ i ] instanceof ShapeNode ) {
				objects[ i ].xform.setParent( groupNode );
			}
		}
		return groupNode;
	}

	static find( name ) {
		for( let r=0; r < XformNode.Roots.length; r++ ) {
			let result = XformNode.Roots[ r ].findNode( name );
			if( result != null ) {
				return result;
			}
		}
		return null;
	}

	static showAll() {
		for( let r=0; r < XformNode.Roots.length; r++ ) {
			XformNode.Roots[ r ].showTree( 0 );
		}
	}

	static getAllSameBaseName( baseName ) {
		let counterList = [];
		for( let i=0; i < XformNode.AllNodes.length; i++ ) {

			let [bName, counter] = XformNode.AllNodes[ i ]
				.name.match(/(.*?)(\d+)?$/i)
				.slice(1);

			if( bName == baseName ) {
				counterList.push( XformNode.AllNodes[ i ] );
			}
		}
		return counterList;
	}
	static validateName( name ) {
		/**
		* Based on a convention I came up with for file renaming on my own machine
		* https://gitlab.com/apankow1234/backup
		*/
		let newValid            = "";
		let [baseName, counter] = name.match(/(.*?)(\d+)?$/i).slice(1);
		let allCounts           = XformNode.getAllSameBaseName( baseName ).map((el)=>{
				let [baseName, counter] = name.match(/(.*?)(\d+)?$/i).slice(1);
				return parseFloat( counter );
			});
			
		if( allCounts.indexOf( counter ) == -1 )
			return name;
		else {
			let maxCount = Math.max( allCounts );
			return ( baseName + ( maxCount + 1 ).toString() )
		}
	}

}

XformNode.deg2Rad  = ( Math.PI / 180.0 );
XformNode.AllNodes = [];
XformNode.Roots    = [];