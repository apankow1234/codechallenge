class ShapeNode extends Node {
	
	constructor( name, meshData = null ) {
		super( name );
		ShapeNode.AllNodes.push( this );

		this.xform = new XformNode( name );
		this.xform.setShape( this )
		this.name = ( this.xform.name + "Shape" );

		this.mesh = [];
		if( meshData )
			this.mesh = meshData;
	}

	prerender() {
		this.xform.updateMatrix();
		return this;
	}

	static showAll() {
		for( let i=0; i < ShapeNode.AllNodes.length; i++ ) {
			let thisNode = ShapeNode.AllNodes[ i ];
			console.log( thisNode.name + "  [ " + thisNode.xform.name + " ]" );
		}
	}

	static removeNode( shapeNode ) {
		let idx = ShapeNode.AllNodes.indexOf( shapeNode );
		if(idx >= 0) {
			this.xform.removeNode();
			ShapeNode.AllNodes.splice( idx, 1 );
		}

	}

}

ShapeNode.AllNodes = [];