class CameraController {
	
	constructor( gl, cameraShapeNode ) {
		this.canvas        = gl.canvas;
		this.camera        = cameraShapeNode;
		let me             = this;
		let bb             = this.canvas.getBoundingClientRect();

		this.rotationRate  = -300;
		this.panRate       = 5;
		this.zoomRate      = 200;

		this.offset        = new Vec2( bb.left, bb.top );
		this.initial       = new Vec2();
		this.previous      = new Vec2();

		this.onUpHandler   = function(e) {me.onMouseUp(e);}
		this.onMoveHandler = function(e) {me.onMouseMove(e)};
		this.canvas.addEventListener( "mousedown",  function(e) { me.onMouseDown(e)}  );
		this.canvas.addEventListener( "mousewheel", function(e) { me.onMouseWheel(e)} );

		// console.log( "I like to move it move it...", this );
	}

	getMouseVec( e ) {
		return new Vec2( e.pageX - this.offset.x, e.pageY - this.offset.y );
	}

	onMouseDown( e ) {
		this.initial = this.previous = this.getMouseVec( e );
		this.canvas.addEventListener( "mouseup",   this.onUpHandler   );
		this.canvas.addEventListener( "mousemove", this.onMoveHandler );
	}

	onMouseUp( e ) {
		this.canvas.removeEventListener( "mouseup",   this.onUpHandler   );
		this.canvas.removeEventListener( "mousemove", this.onMoveHandler );
	}

	onMouseWheel( e ) {
		let d = Math.max( -1, Math.min( 1, ( e.wheelDelta || -e.detail ) ) );
		this.camera.panZ( -d * ( this.zoomRate / this.canvas.height ) );
		// console.log( "zooming", d, this.camera.xform.position.c   );
	}

	onMouseMove( e ) {
		let d  = this.getMouseVec( e );
		let dx = d.sub( this.previous );
		if( !e.shiftKey ) {
			let newVec = dx.mult( new Vec2(
				( this.rotationRate / this.canvas.width  ),
				( this.rotationRate / this.canvas.height )
			) );
			this.camera.xform.addRotation( newVec.y, newVec.x, 0 );
			// console.log( "Orbiting", d.c, this.camera.xform.position.c  );
		} else {
			this.camera.panX( -dx.x * ( this.panRate / this.canvas.width  ) );
			this.camera.panY(  dx.y * ( this.panRate / this.canvas.height ) );
			// console.log( "Panning", d.c, this.camera.xform.position.c   );
		}

		this.previous = d;
	}
	
}