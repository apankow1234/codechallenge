class BasicShader extends Shader{
	constructor( gl, perspMatrix ){

		let fold = ( perspMatrix ) ? 'uPerspMatrix * uCameraMatrix * ' : '';

		var vertShaderText = '#version 300 es\n' +
			'in vec3 a_position;' +
			'in vec2 a_uv;' +
			'uniform  mat4 uPerspMatrix;' +
			'uniform  mat4 uModelViewMatrix;' +
            'uniform  mat4 uCameraMatrix;' +
			'out vec2 uv;' +
			'void main( void ){' +
				'uv = a_uv;' +
				'gl_Position = '+fold+'uModelViewMatrix * vec4( a_position, 1.0 );' +
			'}';
		var fragShaderText = '#version 300 es\n' +
			'precision mediump float;' +
			'in vec2 uv;' +
			'out vec4 finalColor;' +
            'void main(void){ '+
                'float c = ( uv.x <= 0.1 || uv.x >= 0.9 || uv.y <= 0.1 || uv.y >= 0.9 ) ? 0.0 : 1.0;' +
                'finalColor = vec4( c, c, c, 1.0 - c );'+
            '}';
            
		super( gl, vertShaderText, fragShaderText );

		//Standrd Uniforms
		if( perspMatrix )
			this.setPersp( perspMatrix );
            
		//Cleanup
		gl.useProgram(null);
    }
}
