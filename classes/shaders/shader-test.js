class TestShader extends Shader {

	constructor( gl, colorArray ) {
		var vertShaderText = ShaderUtil.getShaderByDomId( "vertShader" );
		var fragShaderText = ShaderUtil.getShaderByDomId( "fragShader" );
		super( gl, vertShaderText, fragShaderText );

		var uColor = gl.getUniformLocation( this.program, "uColor" );
		gl.uniform3fv( uColor, colorArray )

		// this.uniformAddr.pointSize = gl.getUniformLocation( this.program, "u_pointSize" );
		// this.uniformAddr.angle     = gl.getUniformLocation( this.program, "u_angle"     );

		gl.useProgram( null );
	}

	// set( size, angle ) {
	// 	this.gl.uniform1f( this.uniformAddr.pointSize, size  );
	// 	this.gl.uniform1f( this.uniformAddr.angle,     angle );
	// 	return this;
	// }

}