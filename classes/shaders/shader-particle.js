class ParticleShader extends Shader{
	constructor( gl, perspMatrix ){

		let fold = ( perspMatrix ) ? 'uPerspMatrix * uCameraMatrix * ' : '';

		var vertShaderText = '#version 300 es\n' +
			'in vec3 a_position;' +
			'in vec2 a_uv;' +
			'uniform  mat4 uPerspMatrix;' +
			'uniform  mat4 uModelViewMatrix;' +
            'uniform  mat4 uCameraMatrix;' +
			'out vec2 uv;' +
			'void main( void ){' +
				'uv = a_uv;' +
				'gl_Position = '+fold+'uModelViewMatrix * vec4( a_position, 1.0 );' +
			'}';
		var fragShaderText = '#version 300 es\n' +
			'precision mediump float;' +
			'in vec2 uv;' +
			'out vec4 finalColor;' +
            'void main(void){ '+
				'float c = ( uv.x <= 0.1 || uv.x >= 0.9 || uv.y <= 0.1 || uv.y >= 0.9 ) ? 1.0 : 0.0;' +
                // 'float rVal = ( 0.98 * c );' +
                // 'float gVal = ( 0.90 * c );' +
                'finalColor = vec4( 0.0, 0.0, 0.0, 0.5 );'+
            '}';
            
		super( gl, vertShaderText, fragShaderText );

		//Standrd Uniforms
		if( perspMatrix )
			this.setPersp( perspMatrix );
            
		//Cleanup
		gl.useProgram(null);
	}
	
	renderModel( particles ) {

		for( let i=0; i < particles.length; i++ ) {
			let model = particles[ i ];

			if(model instanceof XformNode)
				model = model.shape;
			
			if( model.hasOwnProperty( 'active' ) && model.active ==true ) {
				this.setModel( model.xform.viewMatrix );
				this.gl.bindVertexArray( model.mesh.vao );
		
				if( model.mesh.noCulling )
					this.gl.disable( this.gl.CULL_FACE );
				if( model.mesh.doBlending )
					this.gl.enable( this.gl.BLEND );
		
				if( model.mesh.indexCount ) {
					this.gl.drawElements( model.mesh.drawMode, model.mesh.indexCount, gl.UNSIGNED_SHORT, 0 );
				} else {
					this.gl.drawArrays( model.mesh.drawMode, 0, model.mesh.vertexCount );
				}
		
				this.gl.bindVertexArray( null );
		
				if( model.mesh.noCulling )
					this.gl.enable( this.gl.CULL_FACE );
				if( model.mesh.doBlending )
					this.gl.disable( this.gl.BLEND );
			}
		}
		return this;
	}
}
