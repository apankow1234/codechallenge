class Shader {
	
	constructor( gl, vertShaderText, fragShaderText ) {
		this.program = ShaderUtil.compileProgramFromText( gl, vertShaderText, fragShaderText, true );

		if( this.program != null ) {
			this.gl = gl;
			gl.useProgram( this.program );

			this.attribAddr  = ShaderUtil.getStdAttribAddrs(  gl, this.program );
			this.uniformAddr = ShaderUtil.getStdUniformAddrs( gl, this.program );
		}
	}

	activate() {
		this.gl.useProgram( this.program );
		return this;
	}

	deactivate() {
		this.gl.useProgram( null );
		return this;
	}


	setPersp( matrixData ) {
		this.gl.uniformMatrix4fv( this.uniformAddr.persp,  false, matrixData );
		return this;
	}

	setModel( matrixData ) {
		this.gl.uniformMatrix4fv( this.uniformAddr.model,  false, matrixData  );
		return this;
	}

	setCamera( matrixData ) {
		this.gl.uniformMatrix4fv( this.uniformAddr.camera, false, matrixData  );
		return this;
	}


	remove() {
		if( this.gl.getParameter( this.gl.CURRENT_PROGRAM ) === this.program ) {
			this.gl.useProgram( null );
		}
		this.gl.deleteProgram( this.program );
	}

	prerender() {

	}

	renderModel( model ) {

		if(model instanceof XformNode)
			model = model.shape;
			
		this.setModel( model.xform.viewMatrix );
		this.gl.bindVertexArray( model.mesh.vao );

		if( model.mesh.noCulling )
			this.gl.disable( this.gl.CULL_FACE );
		if( model.mesh.doBlending )
			this.gl.enable( this.gl.BLEND );

		if( model.mesh.indexCount ) {
			this.gl.drawElements( model.mesh.drawMode, model.mesh.indexCount, gl.UNSIGNED_SHORT, 0 );
		} else {
			this.gl.drawArrays( model.mesh.drawMode, 0, model.mesh.vertexCount );
		}

		this.gl.bindVertexArray( null );

		if( model.mesh.noCulling )
			this.gl.enable( this.gl.CULL_FACE );
		if( model.mesh.doBlending )
			this.gl.disable( this.gl.BLEND );

		return this;
	}

}