class Vec3 extends VecN {

	constructor() {
		super( ...arguments );
		this.c = this.c.slice(0,3);
		if( this.c.length == 0 )
			this.c = Array(3).fill(0);
	}

	dup() {
		return new Vec3( this.c );
	}
	neg() {
		return new Vec3( this.x * -1, this.y * -1, this.z * -1 );
	}
	norm() {
		return this.div( this.length );
	}

	add() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 0, 0, 0 ) );
		return new Vec3( this.x + other.x, this.y + other.y, this.z + other.z );
	}
	addTo() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 0, 0, 0 ) );
		this.x += other.x; this.y += other.y; this.z += other.z;
		return this;
	}
	sub() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 0, 0, 0 ) );
		return new Vec3( this.x - other.x, this.y - other.y, this.z - other.z );
	}
	subFrom() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 0, 0, 0 ) );
		this.x -= other.x; this.y -= other.y; this.z -= other.z;
		return this;
	}
	rsub() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 0, 0, 0 ) );
		return other.sub( this );
	}
	mult() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		return new Vec3( this.x * other.x, this.y * other.y, this.z * other.z );
	}
	multTo() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		this.x *= other.x; this.y *= other.y; this.z *= other.z;
		return this;
	}
	div() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		return new Vec3( this.x / other.x, this.y / other.y, this.z / other.z );
	}
	divFrom() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		this.x /= other.x; this.y /= other.y; this.z /= other.z;
		return this;
	}
	rdiv() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		return other.div( this );
	}
	mod() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		return new Vec3( this.x % other.x, this.y % other.y, this.z % other.z );
	}
	modFrom() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		this.x %= other.x; this.y %= other.y; this.z %= other.z;
		return this;
	}
	rmod() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		return other.mod( this );
	}
	dot() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 1, 1, 1 ) );
		return this.mult( other ).array.reduce( (a,b)=>a+b, 0 );
	}
	dist() {
		let other = new Vec3( VecN.getXYZW( [...arguments], 0, 0, 0 ) );
		let dist = new Vec3(
			( this.x - other.x )*( this.x - other.x ),
			( this.y - other.y )*( this.y - other.y ),
			( this.z - other.z )*( this.z - other.z )
		);
		return Math.sqrt( dist.array.reduce( (a,b)=>a+b, 0 ) );
	}
	lerp( fraction, other ) {
		other = ( other instanceof VecN ) ? other : new Vec3( other );
		return this.mult( 1 - fraction ).add( other.mult( fraction ) );
	}
	/**
	* 3 and 7 are the only orthagonally folding dimentions
	* 3D Cross Product <==> Quaternions | Only one way
	* 7D Cross Product <==> Octonions   | Many options
	*/
	cross() {
		let other = ( arguments[0] instanceof VecN ) ? arguments[0] : new Vec3( arguments );
		return new Vec3( 
			( this.y * Other.z - this.z * Other.y ),
			( this.z * Other.x - this.x * Other.z ),
			( this.x * Other.y - this.y * Other.x )
		);

	}

	set x( val )        { if( val ) this.c[0] = parseFloat( val ) || 0.0; }
	get x()             { return this.c[ 0 ]; }
	set y( val )        { if( val ) this.c[1] = parseFloat( val ) || 0.0; }
	get y()             { return this.c[ 1 ]; }
	set z( val )        { if( val ) this.c[2] = parseFloat( val ) || 0.0; }
	get z()             { return this.c[ 2 ]; }

	set( x, y, z ) {
		if(x) this.x = x;
		if(y) this.y = y;
		if(z) this.z = z;
		return this;
	}
}