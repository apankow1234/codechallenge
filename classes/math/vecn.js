class VecN {

	constructor() {
		this.c = [];
		if( Array.isArray( arguments[0] ) ) 
			this.c = arguments[0];
		else if( arguments )
			this.c = [...arguments];
	}

	norm() {
		return this.div( this.length );
	}

	arithmetic( callback, other ) {
		other = ( arguments[1] instanceof VecN ) ? arguments[1] : new VecN( [...arguments].slice(1) );
		let areSameOrder = other.order == this.order;
		let newVec = [];
		for( let i=0; i<this.order; i++ ) {
			if( areSameOrder ) {
				newVec.push( callback( this.c[ i ], other.c[ i ] ) );
			} else if( other.order == 1 ) {
				newVec.push( callback( this.c[ i ], other.c[ 0 ] ) ); /* Converted to vector above */
			} else {
				return;
			}
			return new VecN( newVec );
		}
	}

	dup() {
		return new VecN(this.c);
	}
	neg() {
		return this.mult( -1 );
	}
	add() {
		return this.arithmetic( (a,b)=>{ return a + b; }, ...arguments );
	}
	sub() {
		return this.arithmetic( (a,b)=>{ return a - b; }, ...arguments );
	}
	rsub() {
		return this.arithmetic( (a,b)=>{ return b - a; }, ...arguments );
	}
	mult() {
		return this.arithmetic( (a,b)=>{ return a * b; }, ...arguments );
	}
	div() {
		return this.arithmetic( (a,b)=>{ return a / b; }, ...arguments );
	}
	rdiv() {
		return this.arithmetic( (a,b)=>{ return b / a; }, ...arguments );
	}
	mod() {
		return this.arithmetic( (a,b)=>{ return a % b; }, ...arguments );
	}
	rmod() {
		return this.arithmetic( (a,b)=>{ return b % a; }, ...arguments );
	}
	dot() {
		return this.mult( ...arguments ).array.reduce( (a,b)=>a+b, 0 );
	}
	dist() {
		let d = this.arithmetic( (a,b)=>{ return ( a - b ) * ( a - b ); }, ...arguments );
		return Math.sqrt( d.array.reduce( (a,b)=>a+b, 0 ) );
	}
	lerp( fraction, other ) {
		other = ( arguments[1] instanceof VecN ) ? arguments[1] : new VecN( [...arguments].slice(1) );
		if( this.n == other.n ) {
			return this.mult( 1 - fraction ).add( other.mult( fraction ) );
		} else {
			return;
		}
	}

	eq() {
		let other;
		if( arguments[0] instanceof VecN && arguments[0].n == this.n )
			other = arguments[0].c;
		else if( Array.isArray( arguments[0] ) && arguments[0].length == this.n ) 
			other = arguments[0];
		else if( arguments.length = this.n )
			other = arguments;
		else 
			return false;

		let mine  = this.c;
		for( let i=0; i < this.n; i++ ) {
			if( mine[i] != other[i] )
				return false;
		}
		return true;
	}

	static getXYZW( xyzArgs, defaultX=0, defaultY=0, defaultZ=0, defaultW=0 ) {
		if( xyzArgs.length == 1 )
			if( xyzArgs[0] instanceof VecN )
				return xyzArgs[0].c.slice(0,4);
			else if( Array.isArray( xyzArgs[0] ) && xyzArgs[0].length >= 4 )
				return [ 
					xyzArgs[ 0 ][ 0 ] || defaultX, 
					xyzArgs[ 0 ][ 1 ] || defaultY,
					xyzArgs[ 0 ][ 2 ] || defaultZ,
					xyzArgs[ 0 ][ 3 ] || defaultW
				];
		else
			return [ 
				xyzArgs[ 0 ] || defaultX, 
				xyzArgs[ 1 ] || defaultY,
				xyzArgs[ 2 ] || defaultZ,
				xyzArgs[ 3 ] || defaultW
			];
	}

	get array()      { return this.c;                        }
	get floatArray() { return Float32Array( this.c );        }
	get order()      { return this.c.length;                 }
	get n()          { return this.c.length;                 }
	get mag()        { return Math.sqrt( this.dot( this ) ); }
	get length()     { return Math.sqrt( this.dot( this ) ); }
	get min()        { return Math.min( this.array );        }
	get max()        { return Math.max( this.array );        }
}