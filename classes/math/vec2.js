class Vec2 extends VecN {

	constructor() {
		super( ...arguments );
		this.c = this.c.slice(0,2);
		if( this.c.length == 0 )
			this.c = Array(2).fill(0);
	}

	dup() {
		return new Vec2( this.c );
	}
	neg() {
		return new Vec2( this.x * -1, this.y * -1 );
	}
	add() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 0, 0 ) );
		return new Vec2( this.x + other.x, this.y + other.y );
	}
	addTo() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 0, 0 ) );
		this.x += other.x; this.y += other.y;
		return this;
	}
	sub() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 0, 0 ) );
		return new Vec2( this.x - other.x, this.y - other.y  );
	}
	subFrom() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 0, 0 ) );
		this.x -= other.x; this.y -= other.y;
		return this;
	}
	rsub() {
		let other  = new Vec2( VecN.getXYZW( [...arguments], 0, 0 ) );
		return other.sub( this );
	}
	mult() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		return new Vec2( this.x * other.x, this.y * other.y );
	}
	multTo() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		this.x *= other.x; this.y *= other.y;
		return this;
	}
	div() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		return new Vec2( this.x / other.x, this.y / other.y );
	}
	divFrom() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		this.x /= other.x; this.y /= other.y; 
		return this;
	}
	rdiv() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		return other.div( this );
	}
	mod() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		return new Vec2( this.x % other.x, this.y % other.y );
	}
	modFrom() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		this.x %= other.x; this.y %= other.y; 
		return this;
	}
	rmod() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		return other.mod( this );
	}
	dot() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 1, 1 ) );
		return this.mult( other ).array.reduce( (a,b)=>a+b, 0 );
	}
	dist() {
		let other = new Vec2( VecN.getXYZW( [...arguments], 0, 0 ) );
		let dist = new Vec2(
			( this.x - other.x )*( this.x - other.x ),
			( this.y - other.y )*( this.y - other.y )
		);
		return Math.sqrt( dist.array.reduce( (a,b)=>a+b, 0 ) );
	}
	lerp( fraction, other ) {
		other = ( other instanceof VecN ) ? other : new Vec2( other );
		return this.mult( 1 - fraction ).add( other.mult( fraction ) );
	}

	set x( val )        { if( val ) this.c[0] = parseFloat( val ) || 0.0; }
	get x()             { return this.c[ 0 ]; }
	set y( val )        { if( val ) this.c[1] = parseFloat( val ) || 0.0; }
	get y()             { return this.c[ 1 ]; }

	set( x, y ) {
		if(x) this.x = x;
		if(y) this.y = y;
		return this;
	}
}