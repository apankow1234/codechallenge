class Vec4 extends VecN {
	
	constructor() {
		super( ...arguments );
		this.c = this.c.slice(0,4);
		if( this.c.length == 0 )
			this.c = Array(4).fill(0);
	}

	dup() {
		return new Vec4( this.c );
	}
	neg() {
		return new Vec4( this.x * -1, this.y * -1, this.z * -1, this.w * -1 );
	}
	add() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 0, 0, 0, 0 ) );
		return new Vec4( this.x + other.x, this.y + other.y, this.z + other.z, this.w + other.w );
	}
	addTo() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 0, 0, 0, 0 ) );
		this.x += other.x; this.y += other.y; this.z += other.z; this.w += other.w;
		return this;
	}
	sub() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 0, 0, 0, 0 ) );
		return new Vec4( this.x - other.x, this.y - other.y, this.z - other.z, this.w - other.w  );
	}
	subFrom() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 0, 0, 0, 0 ) );
		this.x -= other.x; this.y -= other.y; this.z -= other.z; this.w -= other.w;
		return this;
	}
	rsub() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 0, 0, 0, 0 ) );
		return other.sub( this );
	}
	mult() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		return new Vec4( this.x * other.x, this.y * other.y, this.z * other.z, this.w * other.w );
	}
	multTo() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		this.x *= other.x; this.y *= other.y; this.z *= other.z; this.w *= other.w;
		return this;
	}
	div() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		return new Vec4( this.x / other.x, this.y / other.y, this.z / other.z, this.w / other.w );
	}
	divFrom() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		this.x /= other.x; this.y /= other.y; this.z /= other.z; this.w /= other.w;
		return this;
	}
	rdiv() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		return other.div( this );
	}
	mod() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		return new Vec4( this.x % other.x, this.y % other.y, this.z % other.z, this.w % other.w );
	}
	modFrom() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		this.x %= other.x; this.y %= other.y; this.z %= other.z; this.w %= other.w;
		return this;
	}
	rmod() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		return other.mod( this );
	}
	dot() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 1, 1, 1, 1 ) );
		return this.mult( other ).array.reduce( (a,b)=>a+b, 0 );
	}

	dist() {
		let other = new Vec4( VecN.getXYZW( [...arguments], 0, 0, 0, 0 ) );
		let dist = new Vec4(
			( this.x - other.x )*( this.x - other.x ),
			( this.y - other.y )*( this.y - other.y ),
			( this.z - other.z )*( this.z - other.z ),
			( this.w - other.w )*( this.w - other.w )
		);
		return Math.sqrt( dist.array.reduce( (a,b)=>a+b, 0 ) );
	}
	lerp( fraction, other ) {
		other = ( other instanceof VecN ) ? other : new Vec4( other );
		return this.mult( 1 - fraction ).add( other.mult( fraction ) );
	}

	set x( val )        { if( val ) this.c[0] = parseFloat( val ) || 0.0; }
	get x()             { return this.c[ 0 ]; }
	set y( val )        { if( val ) this.c[1] = parseFloat( val ) || 0.0; }
	get y()             { return this.c[ 1 ]; }
	set z( val )        { if( val ) this.c[2] = parseFloat( val ) || 0.0; }
	get z()             { return this.c[ 2 ]; }
	set w( val )        { if( val ) this.c[3] = parseFloat( val ) || 0.0; }
	get w()             { return this.c[ 3 ]; }

	set( x, y, z, w ) {
		if(x) this.x = x;
		if(y) this.y = y;
		if(z) this.z = z;
		if(w) this.w = w;
		return this;
	}
}

Vec4.order = 4;