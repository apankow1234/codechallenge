# CodeChallenge

Table of Contents:
*  [Graphics](#the-graphics-thingy)
*  [Expression Handeling](#expression-handeling)
*  [Keywords](#some-keywords)

## The Graphics Thingy

Uses the Mouse and Shift-Key to navigate.

![Particles in WebGL without any libraries](docs/screenshot-Particles2.png)

Full particle engine:

* Shape instancing (floor-grid as "smoke", basic plane as "projectile")
* Forces
* Kill fields/ bounds
* Emitter with physical location

[Live Demo](https://apankow1234.gitlab.io/codechallenge/)

### The Setup

The Environment

![True 3D without Libraries... just a little help](docs/screenshot-3D.png)

No libraries were used except with help in the Matrix math. 

![True 3D without Libraries... just a little help](docs/screenshot-renderSurface.png)


A Visualization of the DAG (Not fully utilized for this project but capable)

![DAG Visualization](docs/screenshot-DAG.png)


## Expression Handeling

[Live Demo](https://apankow1234.gitlab.io/codechallenge/expressions.html)

Avoided any `eval()` and used no libraries:

![Large Expression](docs/screenshot-expressions-04.png)
![first Expression](docs/screenshot-expressions-00.png)
![second Expression](docs/screenshot-expressions-01.png)
![third Expression](docs/screenshot-expressions-02.png)
![fourth Expression](docs/screenshot-expressions-03.png)

## Some Keywords

Defined by yours truly.

Closure

> Imagine you have a utility screwdriver; this thing comes with every bit, magnet, and light you can imagine needing for any job. There might be hundreds of those bits and each one is very specifically for a certain size screw. You could be on most job sites and only use a select 3 but then called to that one odd-ball where you need bits you’ve not seen since you purchased the kit. You as the user of these parts can easily grab only the components you need or you could grab the whole set… But how would you carry all of those bits? And how would you keep your bits separate from your coworker’s bits? Closure is your personal container for each of your tools and their components thus keeping them separate and well organized.

Hoisting

> Which came first?... The chicken or the egg? This is like when you tell your friend an epic story where you start right into the middle of the action and assume that they already know all of your other friends/characters. If the context is setup just right, your audience will be able to assume who you’re talking about. However, if you create a seriously confusing story, your audience will have to stop you and ask you to give some back story on a character. Hoisting is when you reference/ use a variable before you’ve declared it to be one.

Promises

> This is a slightly poor choice in words to describe the concept but not far off base. This is more like when a coworker tells you they’re headed to the store and asks if you would like anything. You tell them that you want X but would be satisfied with Y if they couldn’t get X. Your coworker embarks and now you’re left to wait and you’re not sure how long they’ll be gone for. In this case, you probably shouldn’t give your coworker a vital-to-your-existence shopping list but something on the order of a coffee or midday munchies. When they arrive, you’ll have something to do with them but you don’t require them right away. Asynchronous events and requests are basically this but somehow managing to send a lot of your coworkers on errands for you. You are going to receive X or Y from them but you’ll likely have to wait on all of them.

XHR

> You are the person on the tarmac who has to wave those lights at jet airplanes. The pilots are trained to understand certain signals; different orders of movements represent different commands to the pilots. You don’t have a radio and the pilot wouldn’t really care to talk with you; they need to talk with the tower more so your only communication are these signals. Let’s assume you had proper training. If you need to send the jet down the side runway to get to the main one, you just need to make the gestures. If you give the wrong command, the jet could be delayed or not be allowed to take off. XHR is the set of signals that you need to send to the server for the server to know what you want it to do. This is different from PHP where you would print out instructions for your pilot, just walk away, and hope that no other aircraft were in the way. Printing out directions ahead of time would probably be acceptable for a mom-and-pop crop-duster since there wouldn’t be a lot of traffic or room for mistake but the moment you have to juggle, constant signaling with XHR becomes the better choice.