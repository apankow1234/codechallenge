var ops = ["*","/","+","-"];

function tokenize(expression) {
	let rawTokens = expression.split("")
		.filter((el)=>(el.match(/[^\s]+/)) ? true : false); /*no blanks, please*/
	
	let operators = [];
	for(let i=0; i < rawTokens.length; i++) {
		if(ops.indexOf(rawTokens[i]) > -1) {
			operators.push(i);
		}
	}

	// if only one op and it's not at the start nor the end,
	// 	there are 3 parts
	// if there are 2 ops and neither are at the start nor end,
	// 	there are 5 parts
	// if there are 3 ops and none are at the start and end,
	// 	there are 7 parts
	if( operators.indexOf('0') == -1 && operators.indexOf(rawTokens.length-1) == -1 ) {
		// console.log("no error");
		operators.unshift(-1);
		operators.push(rawTokens.length);
		// console.log('operators', operators, operators.map((el)=>rawTokens[el]));
		let fixedTokens = [];
		
		let numbers = [];
		let use = operators.slice(1, operators.length-1);
		for(let i=0; i < operators.length-1; i++) {
			fixedTokens.push(parseFloat(rawTokens.slice(operators[i]+1, operators[i+1]).join('')));
			fixedTokens.push(rawTokens[use.shift()]);
		}
		fixedTokens = fixedTokens.filter((el)=>(el) ? true : false);
		
		operators = fixedTokens.filter((el)=>ops.indexOf(el)>-1)
			.map((el)=> fixedTokens.indexOf(el) );

		for(let o=0; o < ops.length; o++) {
			for(let i=0; i < operators.length; i++) {

				if(fixedTokens[operators[i]] == ops[o]) {
					// console.log(i, 'operators[i]', operators[i], 'ops[o]', ops[o]);
					let tempSub    = fixedTokens.slice(operators[i]-1, operators[i]+2);
					// console.log('tempSub', tempSub);
					let tempBefore = fixedTokens.slice(0, operators[i]-1);
					// console.log('tempBefore', tempBefore);
					let tempAfter  = fixedTokens.slice(operators[i]+2);
					// console.log('tempAfter', tempAfter);
					fixedTokens = [...tempBefore, tempSub, ...tempAfter];
				}

			}
		}
		return fixedTokens[0];
	}
	return rawTokens;
}

function calc(a, op, b) {
	if(Array.isArray(a))
		a = calc(...a);
	if(Array.isArray(b))
		b = calc(...b);
	
	let idx = ops.indexOf(op);
	switch(idx) {
		case 0:
			return a * b;
		case 1:
			return a / b;
		case 2:
			return a + b;
		case 3:
			return a - b;
	}
}

// let str1 = "1 + 2";
// let str2 = "42 + 2 - 1";
// let str3 = "5 * 42 + 32 - 54 / 100";


// let strings = [str1, str2, str3];
// for(let i in strings) {
// 	console.log( strings[i] );
// 	let tokens = tokenize(strings[i]);
// 	// console.log( tokens );
// 	let answer = calc(...tokens);
// 	console.log( answer );
// }

