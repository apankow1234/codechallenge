const ATTR_POSITION_NAME = "a_position";
const ATTR_POSITION_ADDR = 0;
const ATTR_NORMAL_NAME   = "a_normal";
const ATTR_NORMAL_ADDR   = 1;
const ATTR_UV_NAME       = "a_uv";
const ATTR_UV_ADDR       = 2;


function GLInstance( canvasID ) {
	var canvas = document.getElementById( canvasID );

	if( !canvas ) { /* Build me a render surface if it doesn't already exist */
		canvas = document.createElement('canvas');
		canvas.innerText = "Your browser does not support HTML5.";
		canvas.id        = canvasID;
		document.body.appendChild( canvas );
	}

	var gl = canvas.getContext( "webgl2" );

	if( !gl ) {
		console.error("WebGL context is not available"); 
		return null
	}

	gl.meshCache = {};

	gl.cullFace(  gl.BACK );
	gl.frontFace( gl.CCW );
	gl.enable(    gl.DEPTH_TEST );
	gl.enable(    gl.CULL_FACE );
	gl.depthFunc( gl.LEQUAL );
	gl.blendFunc( gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA );

	gl.clearColor( 0.008, .573, .706, 1.0 );

	gl.refresh = function() {
		/* Assigning `refresh()` to gl.. thus `this` */
		this.clear( this.COLOR_BUFFER_BIT | this.DEPTH_BUFFER_BIT );

		return this; /* Chaining calls made easy since it's a state setter */
	}

	gl.createArrayBuffer = function( arrayOfFloats, staticDraw=true ) {
		/* The phone connection */
		var newBuffer = this.createBuffer();
		/** gl.ARRAY_BUFFER = Pizza Dude
		* Call the pizza dude 
		*/
		this.bindBuffer( this.ARRAY_BUFFER, newBuffer );
		/** Give the pizza dude your order 
		* gl.ARRAY_BUFFER expects an Array of Data
		* pizza guys expect a pizza order
		* STATIC_DRAW is like a one time order
		* DYNAMIC_DRAW is like a rolling order for a party... keep 'em coming!
		*/
		this.bufferData( this.ARRAY_BUFFER, arrayOfFloats, ( staticDraw ) ? this.STATIC_DRAW : this.DYNAMIC_DRAW );
		/**
		* Hang up the phone.. 
		* you wouldn't wait on hold while they make your food and send it out for delivery
		*/
		this.bindBuffer( this.ARRAY_BUFFER, null );

		return newBuffer;
	}

	gl.createMeshVAO = function( name, idxArray, vertArray, normArray, uvArray, floatsPerVert = 3 ) {
		var returnObj = { drawMode: this.TRIANGLES };

		/* Shake and Bake... create the vao and then bind it right away */
		returnObj.vao = this.createVertexArray();
		/* Binding right away will force vertexAttribPointer/enableVertexAttribArray to save to vao */
		this.bindVertexArray( returnObj.vao );

		/* Vertices */
		if( vertArray !== undefined && vertArray != null ) {
			returnObj.vertexBuffer  = this.createBuffer();
			returnObj.floatsPerVert = floatsPerVert;
			returnObj.vertexCount   = vertArray.length / returnObj.floatsPerVert;

			this.bindBuffer( this.ARRAY_BUFFER, returnObj.vertexBuffer );
			this.bufferData( this.ARRAY_BUFFER, new Float32Array( vertArray ), this.STATIC_DRAW );
			this.enableVertexAttribArray( ATTR_POSITION_ADDR );
			this.vertexAttribPointer( ATTR_POSITION_ADDR, 3, this.FLOAT, false, 0, 0 );
		}

		/* Normals */
		if( normArray !== undefined && normArray != null ) {
			returnObj.normalBuffer  = this.createBuffer();
			this.bindBuffer( this.ARRAY_BUFFER, returnObj.normalBuffer );
			this.bufferData( this.ARRAY_BUFFER, new Float32Array( normArray ), this.STATIC_DRAW );
			this.enableVertexAttribArray( ATTR_NORMAL_ADDR );
			this.vertexAttribPointer( ATTR_NORMAL_ADDR, 3, this.FLOAT, false, 0, 0 );
		}

		/* UVs */
		if( uvArray !== undefined && uvArray != null ) {
			returnObj.uvBuffer  = this.createBuffer();
			this.bindBuffer( this.ARRAY_BUFFER, returnObj.uvBuffer );
			this.bufferData( this.ARRAY_BUFFER, new Float32Array( uvArray ), this.STATIC_DRAW );
			this.enableVertexAttribArray( ATTR_UV_ADDR );
			this.vertexAttribPointer( ATTR_UV_ADDR, 2, this.FLOAT, false, 0, 0 );
		}

		/* Indices */
		if( idxArray !== undefined && idxArray != null ) {
			returnObj.idxBuffer   = this.createBuffer();
			returnObj.indexCount  = idxArray.length;
			this.bindBuffer( this.ELEMENT_ARRAY_BUFFER, returnObj.idxBuffer );
			this.bufferData( this.ELEMENT_ARRAY_BUFFER, new Uint16Array(idxArray), this.STATIC_DRAW );
		}

		/* Lock up when you're done so nothing else can be slipped in later */
		this.bindVertexArray( null );
		this.bindBuffer( this.ARRAY_BUFFER, null );
		if( idxArray !== undefined && idxArray != null ) {
			/* Needs to be done after unsetting the other buffers */
			this.bindBuffer( this.ELEMENT_ARRAY_BUFFER, null );
		}

		this.meshCache[ name ] = returnObj;
		return returnObj
	}

	gl.resize = function( w, h ) {
		/* Assigning `resize()` to gl.. thus `this` */
		this.canvas.style.width  = w + 'px';
		this.canvas.style.height = h + 'px';
		this.canvas.width        = w;
		this.canvas.height       = h;
		this.viewport( 0, 0, w, h );

		return this; /* Chaining calls made easy since it's a state setter */
	}

	return gl; /* return us this setup object */
}